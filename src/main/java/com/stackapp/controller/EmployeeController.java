package com.stackapp.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stackapp.model.EmployeeModel;

@RestController
@RequestMapping(value="/employee")
public class EmployeeController {
	
	@RequestMapping(value="/create", method= RequestMethod.POST, consumes= {"application/json"}, produces = {"application/json"})
	public void createEmployee(@RequestBody EmployeeModel employeeModel) {
		
		System.out.println("Employee Data" + employeeModel.toString());  

		
	}
	
	@RequestMapping(value="/update", method= RequestMethod.POST, consumes= {"application/json"}, produces = {"application/json"})
	public void updateEmployee() {

		
	}

	
	
}
