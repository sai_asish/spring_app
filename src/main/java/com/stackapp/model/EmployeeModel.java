package com.stackapp.model;

public class EmployeeModel {

	private Integer employeeId;
	private String emplloyeeName;
	private String employeeAdd;
	private String employeePhoneNo;
	private String employeeSalary;
	
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmplloyeeName() {
		return emplloyeeName;
	}
	public void setEmplloyeeName(String emplloyeeName) {
		this.emplloyeeName = emplloyeeName;
	}
	public String getEmployeeAdd() {
		return employeeAdd;
	}
	public void setEmployeeAdd(String employeeAdd) {
		this.employeeAdd = employeeAdd;
	}
	public String getEmployeePhoneNo() {
		return employeePhoneNo;
	}
	public void setEmployeePhoneNo(String employeePhoneNo) {
		this.employeePhoneNo = employeePhoneNo;
	}
	public String getEmployeeSalary() {
		return employeeSalary;
	}
	public void setEmployeeSalary(String employeeSalary) {
		this.employeeSalary = employeeSalary;
	}
	
	
	@Override
	public String toString() {
		return "EmployeeModel [employeeId=" + employeeId + ", emplloyeeName=" + emplloyeeName + ", employeeAdd="
				+ employeeAdd + ", employeePhoneNo=" + employeePhoneNo + ", employeeSalary=" + employeeSalary + "]";
	}
	
	
	
}
